package main

import (
	"./app"
	// "./app/controller"
	"./app/controller/api"
	"./app/lib"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	lib.SetSeed()

	context := &app.Env{
		DB:          app.InitDB(),
		PORT:        ":6060",
		UPLOAD_PATH: "public/files",
	}

	mx := mux.NewRouter()

	mx.Handle("/api/messagehandler", app.JwtMiddleware(app.Handler{context, api.ReadMessage})).Methods("POST")
	mx.Handle("/api/test", app.JwtMiddleware(app.Handler{context, api.TestMessage})).Methods("POST")

	/******/

	// go app.HandleMessages()
	log.Println("running on", context.PORT)
	log.Fatal(http.ListenAndServe(context.PORT, mx))

}
