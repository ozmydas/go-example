package model

import (
	_ "errors"
	"log"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

type Message struct {
	Id                                       int
	SenderId, SenderName, Recipient, Message string
	UpdateId, Status, Date, CreatedDatetime  string
}

func (Message) TableName() string {
	return "t_dialog"
}

func ModMessageSave(updateId, senderId, senderName, message, date string, DB *gorm.DB, w http.ResponseWriter, r *http.Request) (interface{}, error) {
	textPlaceholder := "+6280989999"

	// assign post to struct
	row := Message{
		UpdateId:        updateId,
		SenderId:        senderId,
		SenderName:      senderName,
		Date:            date,
		Recipient:       textPlaceholder,
		Message:         message,
		CreatedDatetime: time.Now().Format("2006-01-02 15:04:05"),
	}

	// simpan ke table
	save := DB.Create(&row)
	log.Printf("%+v", save.Error)

	// jika error
	if save.Error != nil {
		return r.PostFormValue, save.Error
	}

	// done
	return save.Value.(*Message), nil
} // end func
