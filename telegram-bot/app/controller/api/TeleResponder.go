package api

import (
	"encoding/json"
	_ "errors"
	"fmt"
	_ "io/ioutil"
	"log"
	"net/http"
	_ "strconv"

	"../../../app"
	"../../lib"
	"../../model"
	_ "github.com/gorilla/mux"
	_ "github.com/thedevsaddam/govalidator"
)

func ReadMessage(e *app.Env, w http.ResponseWriter, r *http.Request) (int, error) {

	// get all post variable
	params := map[string]interface{}{
		"message":   r.PostFormValue("message"),
		"timestamp": r.PostFormValue("timestamp"),
	}

	// save all var to db
	placeholder := "x"
	result, err := model.ModMessageSave(placeholder, placeholder, placeholder, r.PostFormValue("message"), placeholder, e.DB, w, r)

	if err != nil {
		lib.JsonRender(w, false, err.Error(), params, 200)
		return 200, nil
	}

	// show result
	msg := "hey its work"
	lib.JsonRender(w, true, msg, result, 200)
	return 200, nil
} // end func

func TestMessage(e *app.Env, w http.ResponseWriter, r *http.Request) (int, error) {
	var t map[string]interface{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&t)
	// err := json.Unmarshal([]byte(r.Body), &t)
	if err != nil {
		panic(err)
	}

	// just a placeholder
	placeholder := "x"

	// parsing here
	tcode := t
	// from := tcode["message"]["from"]
	from := tcode["message"].(map[string]interface{})["from"].(map[string]interface{})
	senderId := from["id"]
	senderName := from["username"]
	updateId := tcode["update_id"].(float64)
	date := placeholder

	strSenderId := fmt.Sprintf("%f", senderId)
	strSenderName := fmt.Sprintf("%f", senderName)
	strUpdateId := fmt.Sprintf("%f", updateId)

	// encode back so we get string
	encodedData, err := json.Marshal(t)
	if err != nil {
		lib.JsonRender(w, false, err.Error(), r.Body, 200)
		return 200, nil
	}

	/* save telegram response to db */
	jsonString := string(encodedData)
	result, err := model.ModMessageSave(strUpdateId, strSenderId, strSenderName, jsonString, date, e.DB, w, r)

	if err != nil {
		lib.JsonRender(w, false, err.Error(), r.Body, 200)
		return 200, nil
	}

	log.Println(jsonString)
	/**/

	lib.JsonRender(w, true, "test", result, 200)
	return 200, nil
} // end func
