-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `t_dialog`;
CREATE TABLE `t_dialog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_id` varchar(128) NOT NULL,
  `sender_id` varchar(128) NOT NULL,
  `sender_name` varchar(128) NOT NULL,
  `sender_number` varchar(128) DEFAULT NULL,
  `recipient` varchar(16) NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(12) NOT NULL DEFAULT 'unread',
  `date` varchar(128) NOT NULL,
  `created_datetime` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2019-12-14 13:05:22
