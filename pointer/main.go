package main

import (
	"fmt"
)

func main() {
	/* DEKLARASI */
	var a int  // variable A dengan type int
	var b int  // variable B dengan type int
	var c *int // variable C dengan isi address dari variable lain yg bertipe int

	/* SET VALUE */
	a = 100
	b = a
	c = &a

	/* OUTPUT */
	fmt.Println(a)
	fmt.Println(&b)
	fmt.Println(c)  // yg ditampilkan adalah addressnya
	fmt.Println(*c) // menampilkan value dari address bersangkutan

	/* UBAH */

	fmt.Println("===========")
	a = 50 // ubah nilai a jadi 50, maka nilai c juga berubah

	/* OUTPUT */
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(*c) // nilai c ikut berubah karena mengikuti address dari a

	/* UBAH */

	fmt.Println("===========")
	*c = 10 // ubah address dari c maka address yg sama (dalam hal ini a) juga ikut berubah

	/* OUTPUT */
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(*c) // nilai c ikut berubah karena mengikuti address dari a
}
