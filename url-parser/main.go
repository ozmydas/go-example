package main

import (
	"fmt"
	"net"
	"net/url"
)

func main() {
	var links = []string{"https://analytics.google.com/analytics/web/#embed/report-home/a98705171w145119383p149829595/",
		"jdbc:mysql://test_user:ouupppssss@localhost:3306/sakila?profileSQL=true",
		"https://bob:pass@testing.com/country/state",
		"http://www.golangprograms.com/",
		"mailto:John.Mark@testing.com",
		"https://www.google.com/search?q=golang+print+string+10+times&oq=golang+print+string+10+times&aqs=chrome..69i57.8786j0j8&sourceid=chrome&ie=UTF-8",
		"urn:oasis:names:description:docbook:dtd:xml:4.1.2",
		"https://stackoverflow.com/jobs?med=site-ui&ref=jobs-tab",
		"ssh://mark@testing.com",
	}

	// fmt.Println(links)

	for _, link := range links {
		fmt.Println("* URL:", link)

		u, err := url.Parse(link)
		if err != nil {
			fmt.Println("\t- Error:", err)
			continue
		}

		parseURL(u)
		fmt.Println()
	}
} // end func

func parseURL(u *url.URL) {
	fmt.Println("\t- Scheme:", u.Scheme)

	if u.Host != "" {
		if host, port, err := net.SplitHostPort(u.Host); err == nil {
			fmt.Println("\t- Host:", host)
			fmt.Println("\t- Port:", port)
		} else {
			fmt.Println("\t- Host:", u.Host)
		}
	}

	if u.Opaque != "" {
		fmt.Println("\t- Opaque:", u.Opaque)
	}

	if u.Path != "" {
		fmt.Println("\t- Path:", u.Path)
	}

	if u.RawQuery != "" {
		fmt.Println("\t- Query:", u.RawQuery)
		queries, err := url.ParseQuery(u.RawQuery)
		if err == nil {
			for key, value := range queries {
				fmt.Println("\t\t-", key, ":", value)
			}
		}
	}

	if u.Fragment != "" {
		fmt.Println("\t- Fragment:", u.Fragment)
	}

	if u.User != nil {
		fmt.Println("\t- Username:", u.User.Username())
		if pwd, ok := u.User.Password(); ok {
			fmt.Println("\t- Password:", pwd)
		}
	}

} // end func
