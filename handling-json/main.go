package main

import (
	"encoding/json"
	"fmt"
)

var JSON = `{
    "name":"Mark Taylor",
    "jobtitle":"Software Developer",
    "phone":{
        "home":"123-466-799",
        "office":"564-987-654"
    },
    "email":"markt@gmail.com"
}`

func main() {
	// encode object to json
	JSON, err := encode()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// show json as string
	fmt.Println("JSON as string :")
	fmt.Println(JSON)

	fmt.Println("=============")

	// decode json back to object
	data := decode(JSON)

	fmt.Println("JSON back to object")
	fmt.Printf("data : %+v\n", data)
	fmt.Println("email :", data["email"])
	fmt.Println("phone > home :", data["phone"].(map[string]interface{})["home"])
} // end func

/*** ENCODE AND DECODE BELOW ***/

func decode(jsonString string) map[string]interface{} {
	var info map[string]interface{}
	json.Unmarshal([]byte(jsonString), &info)

	return info
} // end func

func encode() (string, error) {
	data := make(map[string]interface{})

	data["name"] = "Mark Taylor"
	data["jobtitle"] = "Software Developer"
	data["phone"] = map[string]interface{}{
		"home":   "123-466-799",
		"office": "564-987-654",
	}
	data["email"] = "markt@gmail.com"

	encodedData, err := json.Marshal(data)
	if err != nil {
		return "", err
	}

	jsonString := string(encodedData)
	return jsonString, nil
} // end func
