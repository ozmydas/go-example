package main

import (
	"fmt"
	"net/http"
)

/******/

type UrlStatus struct {
	url    string
	status bool
}

/******/

func main() {

	urls := []string{
		"http://google.com",
		"http://twitter.com",
		"http://facebook.com",
		"http://t-hisyam.net",
	}

	c := make(chan UrlStatus)

	for _, url := range urls {
		go checkUrl(url, c)
	}

	result := make([]UrlStatus, len(urls))

	for i, _ := range result {
		result[i] = <-c

		if result[i].status {
			fmt.Println(result[i].url, "is up.")
		} else {
			fmt.Println(result[i].url, "is down.")
		}
	}

}

func checkUrl(url string, c chan UrlStatus) {
	_, err := http.Get(url)

	if err != nil {
		c <- UrlStatus{url, false}
	} else {
		c <- UrlStatus{url, true}
	}
}
