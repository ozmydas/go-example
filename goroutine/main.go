package main

import (
	"fmt"
	_ "time"
)

func main() {
	// n := 3
	in := make(chan int)
	out := make(chan int)

	go multiplyByTwo(in, out)
	go multiplyByTwo(in, out)
	go multiplyByTwo(in, out)

	in <- 1
	in <- 2
	in <- 3

	fmt.Println(<-out)
	fmt.Println(<-out)
	fmt.Println(<-out)
}

func multiplyByTwo(in <-chan int, out chan<- int) {
	fmt.Println("Initializing goroutine...")

	num := <-in
	result := num * 2

	out <- result
}
