package main

import (
	"fmt"
)

func fibo() func() int {
	a, b := 0, 1

	return func() int {
		if a == 0 {
			a, b = 1, 1
			return 0
		}

		a, b = b, (a + b)
		return a
	}
}

func main() {

	fib := fibo()

	for i := 0; i < 20; i++ {
		fmt.Println(fib())
	}

}
